# GRIP INVEST

# Description

```
Shutdown the port 3000 and 1000 in local machine and do the below steps

Port 3000 - client
Post 1000 - server

You can use npm or yarn package

```

## To Run Locally

```
npm install

npm run start
npm run server
```

## Listen to port

```
http://localhost:3000
```
