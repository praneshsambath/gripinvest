import React, { useState } from "react";
import "./App.css";
import SearchWithSelect from "./common/select";
import { api } from "./service/apiConstants";
import httpClient from "./service/httpClient";
import { ISelectResponse } from "./common/interface/index";

function App() {
  const [data, setData] = useState([]);
  const [value, setValue] = useState([]);
  const [loading, setLoader] = useState(false);

  const handleOnChange = (value:any) => {
    setValue(value);
    setData([]);
    setLoader(false);
  };

  const onSearch = (value: string) => {
    httpClient
      .getInstance()
      .get(api.loadData(value))
      .then((response) => response.data)
      .then((response) => {
        const data = response.map((item: ISelectResponse) => ({
          text: `${item.name} - ${item.price}`,
          value: item.name,
        }));
        setData(data);
        setLoader(false);
      })
      .catch((err) => console.log(err));
  };
  return (
    <div className="App">
      <SearchWithSelect
        loading={loading}
        modes="multiple"
        value={value}
        labelInValue={true}
        placeholder="Select Fruits"
        onSearch={onSearch}
        className="search-with-select"
        onChange={handleOnChange}
        options={data}
      />
    </div>
  );
}

export default App;
