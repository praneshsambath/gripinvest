export interface ISelectResponse {
  id: number;
  name: string;
  price: string;
}
export interface IOptions {
  text: string;
  value: string;
}
export interface ISelectProps {
  loading?: boolean;
  placeholder?: string;
  className?: string;
  onChange: (event: any) => void;
  options: IOptions[];
  labelInValue?: boolean;
  onSearch: (value: string) => void;
  value: string[];
  modes: "multiple" | "tags" | undefined;
}
