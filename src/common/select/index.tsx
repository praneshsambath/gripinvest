import React from "react";
import { Select, Spin } from "antd";
import { ISelectProps } from "../interface";

const { Option } = Select;

export default React.memo(
  ({
    loading,
    onChange,
    options,
    placeholder,
    onSearch,
    value,
    modes,
    className,
    labelInValue,
  }: ISelectProps) => {
    return (
      <React.Fragment>
        <Select
          mode={modes}
          labelInValue={labelInValue}
          value={value}
          placeholder={placeholder}
          notFoundContent={loading && <Spin size="small" />}
          filterOption={true}
          onSearch={onSearch}
          className={className}
          onChange={onChange}
        >
          {options.map((item: any) => (
            <Option key={item.value} value={item.value}>
              {item.text}
            </Option>
          ))}
        </Select>
      </React.Fragment>
    );
  }
);
