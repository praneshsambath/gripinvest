import axios from "axios";

const httpClient = {
  getInstance() {
    const instance = axios.create({
      headers: {
        "Content-Type": "application/json",
      },
      baseURL: "http://localhost:1000/",
    });

    instance.interceptors.response.use(
      (response) => {
        return response;
      },
      (error) => {
        console.log(error, "error");
        if (error.response.status >= 400) {
          // tslint:disable-next-line: no-console
          // console.log(error.response);
          // return error.response.status;
        }
        return Promise.reject(error);
      }
    );

    return instance;
  },
};

export default httpClient;
